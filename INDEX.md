# uHex

uHex is a simple and fast hex editor

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## UHEX.LSM

<table>
<tr><td>title</td><td>uHex</td></tr>
<tr><td>version</td><td>1.0.4a</td></tr>
<tr><td>entered&nbsp;date</td><td>2015-08-27</td></tr>
<tr><td>description</td><td>uHex is a simple and fast hex editor</td></tr>
<tr><td>keywords</td><td>hex,editor,viewer</td></tr>
<tr><td>author</td><td>Mateusz Viste</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://sourceforge.net/projects/uhex</td></tr>
<tr><td>original&nbsp;site</td><td>http://uhex.sourceforge.net</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>Simplified (2-clause) BSD License</td></tr>
</table>
